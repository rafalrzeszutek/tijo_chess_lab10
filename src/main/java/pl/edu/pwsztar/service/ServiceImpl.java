package pl.edu.pwsztar.service;

import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.enums.FigureType;

@org.springframework.stereotype.Service
public class ServiceImpl implements Service{

    @Override
    public boolean isCorrect(FigureMoveDto figureMoveDto) {
        if(FigureType.BISHOP == figureMoveDto.getType()) {
            String[] startPos = figureMoveDto.getStart().split("_");
            String[] destinationPos = figureMoveDto.getDestination().split("_");

            int start_x = startPos[0].charAt(0);
            int destination_x = destinationPos[0].charAt(0);
            int start_y = Integer.parseInt(startPos[1]);
            int destination_y = Integer.parseInt(destinationPos[1]);
            //ruch diagonalny
            return Math.abs(start_x - destination_x) == Math.abs(start_y - destination_y);
        }
        else {
            return false;
        }
    }
}
