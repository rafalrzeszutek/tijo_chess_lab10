package pl.edu.pwsztar.service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;

public interface Service {
    boolean isCorrect (FigureMoveDto figureMoveDto);
}
